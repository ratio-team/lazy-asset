var $ = require("jquery");
require("gsap");

var imagesLoaded = require('imagesloaded');
imagesLoaded.makeJQueryPlugin($);

var LazyAsset = new function() {

	var __isMobileOperatingSystemCache;

	function isMobileOperatingSystem() {

		if (typeof __isMobileOperatingSystemCache !== "undefined") {
			return __isMobileOperatingSystemCache;
		}
		// return true;

	  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

	      // Windows Phone must come first because its UA also contains "Android"
	    if (/windows phone/i.test(userAgent)) {
	    	__isMobileOperatingSystemCache = true;
	        return true;
	    }

	    if (/android/i.test(userAgent)) {
	    	__isMobileOperatingSystemCache = true;
	        return true;
	    }

	    // iOS detection from: http://stackoverflow.com/a/9039885/177710
	    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
	    	__isMobileOperatingSystemCache = true;
	        return true;
	    }

		__isMobileOperatingSystemCache = false;
	    return false;
	}
	// We need to call this on resize and on init of pages which have images in "Contain mode". Reasons why we can't do this by CSS?
	// 1. If we have SVG, and image with max-width 100%, min-width 100%, height: auto, width: auto, then if image is smaller then it doesn’t snap to the placeholder.
	// 2. If we have SVG and div with background-size: contain, then 1px line problem occurs.
	// 3. If we make image as background-size: contain, and make some little transform to make it bigger, OR make SVG smaller, then if we put any layer on it (extra_content), it won’t snap to pixels.
	// Therefore, all solutions suck.

	this.normalizeImagesInContainMode = function(selector) {

		$(selector).find('.lazy-asset-contain').each(function() {

			var width = $(this).data('width');
			var height = $(this).data('height');
			var naturalAspectRatio = width / height;

			var containerWidth = $(this).width();
			var containerHeight = $(this).height();
			var containerAspectRatio = containerWidth / containerHeight;

			var contentContainer = $(this).find('.content-container');

			if (naturalAspectRatio > containerAspectRatio) {

				contentContainer.css({
					width: "100%",
					height: (containerAspectRatio / naturalAspectRatio) * 100 + '%'
				})
			}
			else {
				contentContainer.css({
					width: (naturalAspectRatio / containerAspectRatio) * 100 + '%',
					height: '100%'
				})
			}
		});

	},

	this.playVideo = function(selector) {

		$(selector).find('.lazy-asset-video').each(function() {

			 $(this).addClass('playing');

			var video = $(this).find('video');
			if (video.length > 0) {
				video[0].play();
			}

		})
	},

	this.pauseVideo = function(selector) {

		$(selector).find('.lazy-asset-video').each(function() {
		
			$(this).removeClass('playing');

			var video = $(this).find('video');
			if (video.length > 0) {
				video[0].pause();
			}

		});
	},

	this.load = function(selector, succesCallback, progressCallback) {

		var numberOfVideos = 0;
		var numberOfImages = 0;

		var numberOfVideosLoaded = 0;
		var numberOfImagesLoaded = 0;

		function tryToInvokeCallback() {

			if (typeof progressCallback == "function") { 
				if (numberOfImages + numberOfVideos != 0) {
					progressCallback( (numberOfVideosLoaded + numberOfImagesLoaded) / (numberOfImages + numberOfVideos) );
				} else {
					progressCallback(1);
				}
			}
			
			if (numberOfVideos == numberOfVideosLoaded && numberOfImages == numberOfImagesLoaded) {
				succesCallback();
			}
		}

		var lazyAssetItems = $(selector).find('.lazy-asset').addBack('.lazy-asset');

	    lazyAssetItems.each(function() {

	    	// If image / video is loading or already being loaded, ignore!
    		if ($(this).hasClass('loaded') || $(this).hasClass('loading')) { return; }

    		$(this).addClass('loading');

    		if ($(this).hasClass('lazy-asset-empty')) {
    			$(this).addClass('loaded');
    			return;
    		}

    		var videoSrc = $(this).data('video-src');

    		if (typeof videoSrc !== 'undefined' && videoSrc.length > 0 && !isMobileOperatingSystem()) { // we have video

    			numberOfVideos++;

    			$(this).addClass('video');
    			$(this).find('.lazy-asset-video').append("<video loop muted><source src='" + videoSrc + "'></video>");
    		}
    		else { // we load image

    			numberOfImages++;

    			// desktop src
		        var src = $(this).data('src');

		        // mobile src
		        if (window.innerWidth <= 768) {
		        	src = $(this).data('mobile-src');
		        }

		        $(this).addClass('image');
		        $(this).find('img').attr('src', src);
		        $(this).find('.lazy-asset-background').attr('style', "background-image: url(" + src + ")");

    		}

	    });

	    // Load videos

	    var videos = [];

	    function checkVideosLoadingStatus() {

	    	// console.log('check loading status', videos.length);
	    	var allLoaded = true;

	    	for(var i = 0; i < videos.length; i++) {

	    		var video = videos[i];

    			// console.log('ready state', i, video.readyState);

	    		if (video.readyState < 3) {
	    			allLoaded = false;
	    		}
	    		else {

	    			var item = $(video).closest('.lazy-asset');

	    			if (item.hasClass('loaded')) { continue; }

	    			item.addClass('loaded');

	    			var anim = item.data('anim') || "none";

	    			var videoContainer = item.find('.lazy-asset-video');

	        		if (anim == "fade") {
	        			TweenMax.to(videoContainer, 2, { alpha: 1 });
	        		} else {
	        			TweenMax.set(videoContainer, { alpha: 1 });
	        		}

	        		if (videoContainer.hasClass('playing')) {
	        			videoContainer.find('video')[0].play();
	        		}

	    			numberOfVideosLoaded++;
	    		}
	    	}

	    	if (allLoaded) {
	    		tryToInvokeCallback();
	    	}
	    	else {
	    		setTimeout(checkVideosLoadingStatus, 250);
	    	}
	    }

    	lazyAssetItems.find('video').each(function() {
    		var video = $(this)[0];
    		video.load();
    		videos.push(video);
    	});

    	if (videos.length > 0) {
    		checkVideosLoadingStatus();
    	}

	    // Load images

	    lazyAssetItems.each(function() {

	    	if (!$(this).hasClass('image')) { return; }

	    	var _this = $(this);

	    	_this.imagesLoaded( { 
	    			background: true
    			})
	    		.always(function() {

	    			if (_this.hasClass('loaded')) { return; }

	    			var anim = _this.data('anim') || "none";

	        		if (anim == "fade") {
	        			TweenMax.to(_this.find('img, .lazy-asset-background'), 2, { alpha: 1 });
	        		} else {
	        			TweenMax.set(_this.find('img, .lazy-asset-background'), { alpha: 1 });
	        		}

    				_this.removeClass('loading').addClass('loaded');

    				numberOfImagesLoaded++;

    				tryToInvokeCallback();
	    		}
	    		)
	    		.done(function() {
	    			// console.log('done');
	    		})
	    		.fail(function() {
	    			// console.log('fail');
	    		})

	    });

	    tryToInvokeCallback();
	}
};

module.exports = LazyAsset;

