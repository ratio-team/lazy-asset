<?php

class LazyAsset {

    const MODE_COVER = 0;
    const MODE_CONTAIN = 1;
    const MODE_ASPECT_RATIO = 2;

	private static function prepareOptions($options) {

		if (!array_key_exists("mode", $options)) { trigger_error("lazy asset must have 'mode' property defined"); }
		if ($options["mode"] != self::MODE_COVER && $options["mode"] != self::MODE_CONTAIN && $options["mode"] != self::MODE_ASPECT_RATIO) { trigger_error("lazy asset 'mode' property has wrong value"); }

		if (!array_key_exists("classes", $options)) { $options["classes"] = ""; }
		if (!array_key_exists("animation", $options)) { $options["animation"] = "none"; }
		if (!array_key_exists("extra_markup", $options)) { $options["extra_markup"] = ""; }
		if (!array_key_exists("attributes", $options)) { $options["attributes"] = array(); }

		if (!array_key_exists("empty", $options)) { $options["empty"] = false; }

		if ($options["empty"]) {

			if ($options["mode"] != self::MODE_COVER) {
				trigger_error("lazy asset can't have empty property set to 'true' and be in CONTAIN or ASPECT_RATIO mode at the same time");
			}
			return $options;
		}

		if (!array_key_exists("img", $options)) { trigger_error("lazy asset must have 'img' property defined"); }
		if (!array_key_exists("img-mobile", $options)) { $options["img-mobile"] = $options["img"]; }
		if (!array_key_exists("width", $options)) { trigger_error("lazy asset must have 'width' property defined"); }	
		if (!array_key_exists("height", $options)) { trigger_error("lazy asset must have 'height' property defined"); }

		if (!array_key_exists("video-src", $options)) { $options["video-src"] = ""; }

		$options["extra_markup"] = str_replace("__width__", $options["width"], $options["extra_markup"]);
		$options["extra_markup"] = str_replace("__height__", $options["height"], $options["extra_markup"]);

		return $options;
	}

	private static function pasteAttributes($options) {
		$result = "";

		foreach($options["attributes"] as $key => $value) {
			$result = $result . " " . $key . "=\"" . $value . "\"";
		}

		return $result;
	}

	private static function putCover($options) {

		if ($options["empty"]) {

		?>
			<div class="lazy-asset lazy-asset-cover lazy-asset-empty <?php echo $options["classes"]; ?>" <?= self::pasteAttributes($options); ?> >
			     <div class="lazy-asset-background"></div>
			     <div class="lazy-asset-video"></div>

			     <?= $options["extra_markup"]; ?>
			 </div>
	
		<?php

		} else {

		?>

			<div class="lazy-asset lazy-asset-cover <?php echo $options["classes"]; ?>" data-width="<?php echo $options['width']; ?>" data-height="<?php echo $options['height']; ?>" data-src="<?php echo $options["img"];?>" data-mobile-src="<?php echo $options["img-mobile"]; ?>" data-video-src="<?php echo $options["video-src"]; ?>" data-anim="<?php echo $options["animation"]; ?>" <?= self::pasteAttributes($options); ?> >
			     <div class="lazy-asset-background"></div>
			     <div class="lazy-asset-video"></div>

			     <?= $options["extra_markup"]; ?>
			 </div>

		<?php

		}

	}

	private static function putContain($options) {

		?>

			<div class="lazy-asset lazy-asset-contain <?php echo $options["classes"]; ?>" data-width="<?php echo $options['width']; ?>" data-height="<?php echo $options['height']; ?>" data-src="<?php echo $options["img"];?>" data-mobile-src="<?php echo $options["img-mobile"]; ?>" data-anim="<?php echo $options["animation"]; ?>" <?= self::pasteAttributes($options); ?> >

				<div class="content-container">
					<div class="lazy-asset-background"></div>
			     	<?= $options["extra_markup"]; ?>
				</div>

			 </div>

		<?php
	}

	private static function putAspectRatio($options) {

		?>

			<div class="lazy-asset lazy-asset-aspect-ratio <?php echo $options["classes"]; ?>" data-width="<?php echo $options['width']; ?>" data-height="<?php echo $options['height']; ?>" data-src="<?php echo $options["img"];?>" data-mobile-src="<?php echo $options["img-mobile"]; ?>" data-anim="<?php echo $options["animation"]; ?>" <?= self::pasteAttributes($options); ?> >

				<svg class="placeholder" xmlns="http://www.w3.org/2000/svg" width="<?php echo $options['width']; ?>" height="<?php echo $options['height']; ?>"></svg>

				<div class="content-container">
					<div class="lazy-asset-background"></div>
			     	<?= $options["extra_markup"]; ?>
				</div>
			</div>
		<?php
	}

	public static function put($options) {
		$options = self::prepareOptions($options);

		switch($options["mode"]) {

			case self::MODE_COVER:
				self::putCover($options);
				break;

			case self::MODE_CONTAIN:
				self::putContain($options);
				break;

			case self::MODE_ASPECT_RATIO:
				self::putAspectRatio($options);
				break;

		}

	}

	/** URL ASSETS */

	private static $FILE_BASE_PATH = "";
	private static $FILE_BASE_URL = "";

	public static function setFileBasePath($path) {
		self::$FILE_BASE_PATH = $path;
	}

	public static function setFileBaseUrl($path) {
		self::$FILE_BASE_URL = $path;
	}

	public static function putFile($options) {

		if (!array_key_exists("file-path", $options)) { $options["file-path"] = null; }
		if (!array_key_exists("file-path-mobile", $options)) { $options["file-path-mobile"] = $options["file-path"]; }

		if ($options["file-path"] == null) { // in case of empty wp image object
			$options["empty"] = true;
		}
		else {

			list($width, $height) = getimagesize(self::$FILE_BASE_PATH . $options["file-path"]);

			$fileUrl = self::$FILE_BASE_URL . $options["file-path"];
			$fileMobileUrl = self::$FILE_BASE_URL . $options["file-path-mobile"];

			$options["img"] = $fileUrl;
			$options["img-mobile"] = $fileMobileUrl;
			$options["width"] = $width;
			$options["height"] = $height;

			self::put($options);
		}
		
	}

	/** WORDPRESS ASSETS */

	public static function putWp($options) {

		if (!array_key_exists("wp-img", $options)) { $options["wp-img"] = null; }
		if (!array_key_exists("wp-size", $options)) { $options["wp-size"] = "large"; }
		if (!array_key_exists("wp-size-mobile", $options)) { $options["wp-size-mobile"] = $options["wp-size"]; }

		if ($options["wp-img"] == null) { // in case of empty wp image object
			$options["empty"] = true;
		}
		else {

			$wpImg = $options["wp-img"];

			$options["img"] = $wpImg['sizes'][$options['wp-size']];
			$options["img-mobile"] = $wpImg['sizes'][$options['wp-size-mobile']];
			$options["width"] = $wpImg['sizes'][$options['wp-size']. '-width'];
			$options["height"] = $wpImg['sizes'][$options['wp-size']. '-height'];

		}

		self::put($options);
	}

	/** VIMEO ASSETS */

	public static function putVimeo($options) {

		if (!array_key_exists("vimeo-id", $options)) { trigger_error("deferred image vimeo function must have 'vimeo-id' property defined"); }
		if (!array_key_exists("vimeo-img-only", $options)) { $options["video-img-only"] = false; }
		if (!array_key_exists("vimeo-video-size", $options)) { if (!$options["video-img-only"]) { trigger_error("deferred image vimeo function must have 'video-size' property defined or 'no-video' set to true"); } }
		if (!array_key_exists("vimeo-img-size", $options)) { trigger_error("deferred image vimeo function must have 'img-size' property defined"); }
		if (!array_key_exists("vimeo-img-size-mobile", $options)) { $options["vimeo-img-size-mobile"] = $options["vimeo-img-size"]; }

		$vimeoData = getVimeoData($options["vimeo-id"]); // caches Vimeo data from server or cache

		if ($vimeoData == NULL) {
			$options["empty"] = true;
		}
		else {

			$imgOnly = $vimeoData["videos"][$options["vimeo-img-only"]];

			if (!$imgOnly) {
				$options["video-src"] = $vimeoData["videos"][$options["vimeo-video-size"]]["link"];
			}

			$image = $vimeoData["pictures"][$options["vimeo-img-size"]];
			$imageMobile = $vimeoData["pictures"][$options["vimeo-img-size-mobile"]];
			
			$options["img"] = $image["link"];
			$options["img-mobile"] = $imageMobile["link"];
			$options["width"] = $image["width"];
			$options["height"] = $image["height"];
		}

		self::put($options);
	}

}



