<?php
	require('../lazy-asset.php');
?>


<html lang="en">

	<head>
        <meta charset="UTF-8" />

        <meta name="viewport" content="width=device-width,user-scalable=no,minimum-scale=1,maximum-scale=1">

        <title>Lazy asset demo</title>

		<script src="dist/bundle.js"></script>
		<link rel="stylesheet" href="dist/style.css">
    </head>

	<body>

		<h1>Lazy asset demo</h1>

		<?php

			LazyAsset::setFileBasePath("./");
			LazyAsset::setFileBaseUrl("");

			LazyAsset::putFile([
				"mode"				=> LazyAsset::MODE_COVER,
				"classes" 			=> "photo-cover",
				"animation"			=> "fade",

				"file-path"			=> "cat1.jpg"
			]);

			LazyAsset::putFile([
				"mode"				=> LazyAsset::MODE_ASPECT_RATIO,
				"classes" 			=> "photo-aspect-ratio",
				"animation"			=> "fade",

				"file-path"			=> "cat2.jpg"
			]);

			LazyAsset::putFile([
				"mode"				=> LazyAsset::MODE_CONTAIN,
				"classes" 			=> "photo-contain",
				"animation"			=> "fade",

				"file-path"			=> "cat3.jpg"
			]);

			LazyAsset::putFile([
				"mode"				=> LazyAsset::MODE_COVER,
				"classes" 			=> "video",
				"animation"			=> "fade",

				"video-src"			=> "video.mp4",
				"file-path"			=> "cat3.jpg"
			]);
		?>

	</body>

</html>







