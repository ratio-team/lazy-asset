var $ = require("jquery");
var LazyAsset = require("../lazy-asset.js");

/**
 * Tests to write
 *
 * 1. Video preloading
 * 2. Vimeo integration
 * 
 */

$(document).ready(function() {

	setTimeout(function() {

		LazyAsset.load('.photo-cover', function() {
			}, function(progress) {
		});

	}, 1000);

	setTimeout(function() {

		LazyAsset.load('.photo-aspect-ratio', function() {
		}, function(progress) {
		});

	}, 2000);

	LazyAsset.normalizeImagesInContainMode('body');

	setTimeout(function() {

		LazyAsset.load('.photo-contain', function() {
		}, function(progress) {
		});

	}, 3000);

	setTimeout(function() {

		LazyAsset.playVideo('.video');

		LazyAsset.load('.video', function() {
		}, function(progress) {
		});

	}, 4000);

});
